/*
(function() {
    try {
        var $_console$$ = console;
        Object.defineProperty(window, "console", {
            get: function() {
                if ($_console$$._commandLineAPI)
                    throw "Sorry, for security reasons, the script console is deactivated on netflix.com";
                return $_console$$
            },
            set: function($val$$) {
                $_console$$ = $val$$
            }
        })
    } catch ($ignore$$) {
    }
    eval = undefined;
    console = undefined;
})();
*/
"use strict";
// Avoid `console` errors in browsers that lack a console.
/*(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());*/

// Place any jQuery/helper plugins in here.
jQuery.extend({
    getCachedJSON: function (url, callback) {
        var cacheTimeInMs = 21600000; //6h
        var currentTimeInMs = new Date().getTime();

        var cache = {
            data:null,
            timestamp:null
        };

        if (typeof window.localStorage[url] !== "undefined") {
            cache = JSON.parse(window.localStorage[url]);

            var validCache = (currentTimeInMs - cache.timestamp) < cacheTimeInMs;

            if (validCache) {
                callback(cache.data);
                return true;
            }
        }

        return $.getJSON(url, function (data) {
            cache.data = data;
            cache.timestamp = new Date().getTime();

            window.localStorage[url] = JSON.stringify(cache);

            callback(cache.data);
        });
    }
});
