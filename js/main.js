"use strict";
//(function() {
	var correct = new Audio('snd/right.mp3');
	var incorrect = new Audio('snd/wrong.mp3');
	
    function Quiz(data){
    	var self = this;
    	self.questions = ko.observableArray();

    	data.forEach(function(question){
    		self.questions.push(new Question(question));
    	});

        self.pageNumber = ko.observable(0);
        self.nbPerPage = 1;
        self.totalPages = ko.computed(function() {
            var div = Math.floor(self.questions().length / self.nbPerPage);
            div += self.questions().length % self.nbPerPage > 0 ? 1 : 0;
            return div - 1;
        });

        self.question = ko.computed(function() {
            var first = self.pageNumber() * self.nbPerPage;
            return self.questions.slice(first, first + self.nbPerPage);
        });
        self.hasPrevious = ko.computed(function() {
            return self.pageNumber() !== 0;
        });
        self.hasNext = ko.computed(function() {
            return self.pageNumber() !== self.totalPages();
        });
        self.next = function() {
            if(self.pageNumber() < self.totalPages()) {
                self.pageNumber(self.pageNumber() + 1);
            }
        }

        self.previous = function() {
            if(self.pageNumber() != 0) {
                self.pageNumber(self.pageNumber() - 1);
            }
        }
    }

    function Answer(data){
        var self = this;

        self.id = data.id;
        self.content = ko.observable(data.content);
        self.correct = ko.observable(Boolean(data.correct));
        self.enabled = ko.observable(true);
    }

    function Question(data) {
    	var self = this;

    	self.content= ko.observable(data.content);
    	self.answers= ko.observableArray()

    	data.answers.forEach(function(answer){
    		self.answers.push(new Answer(answer));
    	});

        self.answers().sort(function() { return 0.5 - Math.random() });

    	self.correctAns = findByKeyVal(data.answers,'correct',1);
    	self.isCorrect = function(item, e) {
    		if(self.isAnswered) return null;
    		self.isAnswered = true;
    		$(e.currentTarget).addClass('blink');
    		setTimeout(function(){
    			if(!!item.correct){
    				$(e.currentTarget).removeClass('blink').addClass('is-correct'); 
					correct.play();
				}else{
					incorrect.play()
    				$(e.currentTarget).removeClass('blink').addClass('is-incorrect')
    				.parent().children().eq(self.correctAns).addClass('is-correct');
				}
                setTimeout(function(){
                	self.isAnswered = false;
                	if(quiz.hasNext()) quiz.next();
                	else quiz.pageNumber(0);
                },2000)
    		}, 3100);

    	};
    }

    function findByKeyVal(arraytosearch, key, val) {
    	for (var i = 0; i < arraytosearch.length; i++) {
    		if (arraytosearch[i][key] == val) {
    			return i;
    		}
    	}
    	return null;
    }



    /**************************/

    var quiz;
    $( document ).ready(function() {

        $.getCachedJSON("http://127.0.0.1/quiz/api/v1/questions?expand=answers",function(data) {
            quiz = new Quiz(data);
            ko.applyBindings(quiz);
        })

    });

//}());

/*less performance code
function fiftyFifty(){
    var t0 = performance.now();

    var sort = [];
    var ans = quiz.question()[0].answers();
    if(ans.length<4) return;
    var fRand = null;
    var rand = Math.floor( Math.random() * (ans.length-1) );
    for(var i=0; i<2; i++){
        var rand = Math.floor( Math.random() * (ans.length-1) );
        console.log(ans[rand])
        while(ans[rand].correct() || rand===fRand){
           rand = Math.floor( Math.random() * (ans.length-1) );
        }
        sort.push(ans[rand]);
        fRand = rand;
    }
          var t1 = performance.now();
console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.")
    sort.forEach(function(ans){
        console.log(ans)
        ans.enabled(false)
    })

}
*/
function fiftyFifty(){
    var ans = quiz.question()[0].answers();
    if(ans.length<4) return;
    function exclueCorrect(ans) {
      return  (!ans.correct() && !ans.enable)
    }
    var incorrectAns = ans.filter(exclueCorrect)
        .sort(function() { return 0.5 - Math.random() });

    for(var i=0; i<2; i++){
        incorrectAns[i].enabled(false)
    }

    ans = incorrectAns = null;
}



//var _0x28c0=["\x75\x73\x65\x20\x73\x74\x72\x69\x63\x74","\x71\x75\x65\x73\x74\x69\x6F\x6E\x73","\x6F\x62\x73\x65\x72\x76\x61\x62\x6C\x65\x41\x72\x72\x61\x79","\x70\x75\x73\x68","\x66\x6F\x72\x45\x61\x63\x68","\x70\x61\x67\x65\x4E\x75\x6D\x62\x65\x72","\x6F\x62\x73\x65\x72\x76\x61\x62\x6C\x65","\x6E\x62\x50\x65\x72\x50\x61\x67\x65","\x74\x6F\x74\x61\x6C\x50\x61\x67\x65\x73","\x6C\x65\x6E\x67\x74\x68","\x66\x6C\x6F\x6F\x72","\x63\x6F\x6D\x70\x75\x74\x65\x64","\x71\x75\x65\x73\x74\x69\x6F\x6E","\x73\x6C\x69\x63\x65","\x68\x61\x73\x50\x72\x65\x76\x69\x6F\x75\x73","\x68\x61\x73\x4E\x65\x78\x74","\x6E\x65\x78\x74","\x70\x72\x65\x76\x69\x6F\x75\x73","\x63\x6F\x6E\x74\x65\x6E\x74","\x61\x6E\x73\x77\x65\x72\x73","\x69\x73\x41\x6E\x73\x77\x65\x72\x65\x64","\x63\x6F\x72\x72\x65\x63\x74\x41\x6E\x73","\x63\x6F\x72\x72\x65\x63\x74","\x69\x73\x43\x6F\x72\x72\x65\x63\x74","\x62\x6C\x69\x6E\x6B","\x61\x64\x64\x43\x6C\x61\x73\x73","\x63\x75\x72\x72\x65\x6E\x74\x54\x61\x72\x67\x65\x74","\x69\x73\x2D\x63\x6F\x72\x72\x65\x63\x74","\x72\x65\x6D\x6F\x76\x65\x43\x6C\x61\x73\x73","\x65\x71","\x63\x68\x69\x6C\x64\x72\x65\x6E","\x70\x61\x72\x65\x6E\x74","\x69\x73\x2D\x69\x6E\x63\x6F\x72\x72\x65\x63\x74","\x68\x74\x74\x70\x3A\x2F\x2F\x31\x32\x37\x2E\x30\x2E\x30\x2E\x31\x2F\x71\x75\x69\x7A\x2F\x61\x70\x69\x2F\x76\x31\x2F\x71\x75\x65\x73\x74\x69\x6F\x6E\x73\x3F\x65\x78\x70\x61\x6E\x64\x3D\x61\x6E\x73\x77\x65\x72\x73","\x61\x70\x70\x6C\x79\x42\x69\x6E\x64\x69\x6E\x67\x73","\x67\x65\x74\x43\x61\x63\x68\x65\x64\x4A\x53\x4F\x4E","\x72\x65\x61\x64\x79"];var _0x9077=[_0x28c0[0],_0x28c0[1],_0x28c0[2],_0x28c0[3],_0x28c0[4],_0x28c0[5],_0x28c0[6],_0x28c0[7],_0x28c0[8],_0x28c0[9],_0x28c0[10],_0x28c0[11],_0x28c0[12],_0x28c0[13],_0x28c0[14],_0x28c0[15],_0x28c0[16],_0x28c0[17],_0x28c0[18],_0x28c0[19],_0x28c0[20],_0x28c0[21],_0x28c0[22],_0x28c0[23],_0x28c0[24],_0x28c0[25],_0x28c0[26],_0x28c0[27],_0x28c0[28],_0x28c0[29],_0x28c0[30],_0x28c0[31],_0x28c0[32],_0x28c0[33],_0x28c0[34],_0x28c0[35],_0x28c0[36]];_0x9077[0];(function(){function _0x41bdx2(_0x41bdx3){var _0x41bdx4=this;_0x41bdx4[_0x9077[1]]= ko[_0x9077[2]]();_0x41bdx3[_0x9077[4]](function(_0x41bdx5){_0x41bdx4[_0x9077[1]][_0x9077[3]]( new _0x41bdx8(_0x41bdx5))});_0x41bdx4[_0x9077[5]]= ko[_0x9077[6]](0);_0x41bdx4[_0x9077[7]]= 1;_0x41bdx4[_0x9077[8]]= ko[_0x9077[11]](function(){var _0x41bdx6=Math[_0x9077[10]](_0x41bdx4[_0x9077[1]]()[_0x9077[9]]/ _0x41bdx4[_0x9077[7]]);_0x41bdx6+= _0x41bdx4[_0x9077[1]]()[_0x9077[9]]% _0x41bdx4[_0x9077[7]]> 0?1:0;return _0x41bdx6- 1});_0x41bdx4[_0x9077[12]]= ko[_0x9077[11]](function(){var _0x41bdx7=_0x41bdx4[_0x9077[5]]()* _0x41bdx4[_0x9077[7]];return _0x41bdx4[_0x9077[1]][_0x9077[13]](_0x41bdx7,_0x41bdx7+ _0x41bdx4[_0x9077[7]])});_0x41bdx4[_0x9077[14]]= ko[_0x9077[11]](function(){return _0x41bdx4[_0x9077[5]]()!== 0});_0x41bdx4[_0x9077[15]]= ko[_0x9077[11]](function(){return _0x41bdx4[_0x9077[5]]()!== _0x41bdx4[_0x9077[8]]()});_0x41bdx4[_0x9077[16]]= function(){if(_0x41bdx4[_0x9077[5]]()< _0x41bdx4[_0x9077[8]]()){_0x41bdx4[_0x9077[5]](_0x41bdx4[_0x9077[5]]()+ 1)}};_0x41bdx4[_0x9077[17]]= function(){if(_0x41bdx4[_0x9077[5]]()!= 0){_0x41bdx4[_0x9077[5]](_0x41bdx4[_0x9077[5]]()- 1)}}}function _0x41bdx8(_0x41bdx5){var _0x41bdx9=this;_0x41bdx9[_0x9077[18]]= ko[_0x9077[6]](_0x41bdx5[_0x9077[18]]);_0x41bdx9[_0x9077[19]]= ko[_0x9077[2]](_0x41bdx5[_0x9077[19]]);_0x41bdx9[_0x9077[20]]= false;_0x41bdx9[_0x9077[21]]= _0x41bdxb(_0x41bdx5[_0x9077[19]],_0x9077[22],1);_0x41bdx9[_0x9077[23]]= function(_0x41bdx5,_0x41bdxa){if(_0x41bdx9[_0x9077[20]]){return null};_0x41bdx9[_0x9077[20]]= true;$(_0x41bdxa[_0x9077[26]])[_0x9077[25]](_0x9077[24]);setTimeout(function(){if(!!_0x41bdx5[_0x9077[22]]){$(_0x41bdxa[_0x9077[26]])[_0x9077[28]](_0x9077[24])[_0x9077[25]](_0x9077[27])}else {$(_0x41bdxa[_0x9077[26]])[_0x9077[28]](_0x9077[24])[_0x9077[25]](_0x9077[32])[_0x9077[31]]()[_0x9077[30]]()[_0x9077[29]](_0x41bdx9[_0x9077[21]])[_0x9077[25]](_0x9077[27])};setTimeout(function(){_0x41bdx9[_0x9077[20]]= false;if(_0x41bdxf[_0x9077[15]]()){_0x41bdxf[_0x9077[16]]()}else {_0x41bdxf[_0x9077[5]](0)}},2E3)},3100)}}function _0x41bdxb(_0x41bdxc,_0x41bdxd,_0x41bdx5){var _0x41bdxe=0;for(;_0x41bdxe< _0x41bdxc[_0x9077[9]];_0x41bdxe++){if(_0x41bdxc[_0x41bdxe][_0x41bdxd]== _0x41bdx5){return _0x41bdxe}};return null}var _0x41bdxf;$(document)[_0x9077[36]](function(){$[_0x9077[35]](_0x9077[33],function(_0x41bdx10){_0x41bdxf=  new _0x41bdx2(_0x41bdx10);ko[_0x9077[34]](_0x41bdxf)})})})()